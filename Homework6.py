# Напишите функцию которая принимает *args
def function(num1, num2, *args):
    print(num1, num2, *args)


function(130, 230, 100, "500")


# Напишите функцию которая принимает *kwargs
def minimalizacia(word1, word2, word3, **kwargs):
    print(word1 + word2 + word3)
    print(kwargs)


minimalizacia('I', ' am', ' the coolest', admin="name")
# Напишите функцию которая принимает *args и проходит циклом по пришедшмим данным
abc = [{
    'dict': 10,
    'dict2': 'bbbb'
    },
    {
        'dict': 1234,
        'dict2': 4321
    },
    {
        'dict': 100,
        'dict2': 12,
    },
    {
        'dict': 123,
        'dict2': 'dddd'
    }
]

boys = [
    {
        'name': 'Suli',
        'age': 17
    },
    {
        'name': 'Tolu',
        'age': 20
    }
]


def dannye(*args):
    for people in args:
        for person in people:
            print(person)


dannye(abc, boys)

# Напишите имитацию работу клуба (массив девушек, массив парней, охраник, администратор), но с использованием параметров args, kwargs
devushki = [
    {
        'name': 'Azhara',
        'age': 16
    },
    {
        'name': 'Albina',
        'age': 22
    },
    {
        'name': 'Nurkyz',
        'age': 18
    }
]
parni = [
    {
        'name': 'Askat',
        'age': 17
    },
    {
        'name': 'Baiel',
        'age': 20
    },
    {
        'name': 'Ulan',
        'age': 21
    }
]

def club(*args, **kwargs):
    for a in args:
        for i in a:
            if i['age'] >= 18:
                print(kwargs['secure'] + '--- xorosho, prohodish ---', i['name'])
                print(kwargs['admin'] + '--- pechat ---', i['name'] + '\n')

            elif i['age'] <= 18:
                print(kwargs['secure'] + '--- vse ploho, idi spat! ---', i['name'] + '\n')


club(devushki, parni, admin="Admininstartor", secure="Ohrannik")